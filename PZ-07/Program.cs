﻿using Biblioteka;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_07
{
    class Program
    {
        public static void zgina(Postac p)
        {
            Console.WriteLine("Zmarło się "+ p.Imie);
        }
        public static void uniko (Lucznik l)
        {
            Console.WriteLine("Zrobil uniko "+l.Imie);
        }
        public static void ogienOnTheFloor(object sender, Intensywnosc i)
        {
            foreach(Postac p in Druzyna)
            {
                p.odnoszenieObrazen(i.Iletego, Obrazenia.Ogniste);
            }
        }
        public static List<Postac> Druzyna { get; set; }
        static void Main(string[] args)
        {
            Wojownik w = new Wojownik("Bartek");
            Lucznik l = new Lucznik("Andrzej");
            w.onDie += new PostacDelegate(zgina);
            l.onUnik += new Unik(uniko);
            l.onDie += new PostacDelegate(zgina);
            l.SzansaNaUnikniecie += 50;
            Czarodziej czarke = new Czarodziej("Czarek");
            czarke.onDie += new PostacDelegate(zgina);
            Biblioteka.Smok leon = new Biblioteka.Smok
            {
                Imie="Leon",
                Inteligencja=99,
                ManaP=new Mana { Aktualne=999,Maksymalne=999},
                Odpornosc=new List<Odpornosc> { new Odpornosc { Nazwa="to bez sensu"} },
                OdpornoscP=99,
                Poziom=99,
                PunktyDoswiadczenia=99999,
                Sila=99,
                ZadawaneObrazenia=99,
                Zrecznosc=99,
                ZycieP=new Zycie { Aktualne=10000,Maksymalne= 10000 }
            };
            leon.onZioniecie += new EventHandler<Intensywnosc>(ogienOnTheFloor);
            w.Poziom = 10;
            l.Poziom = 5;
            czarke.Poziom = 1;

          
            Druzyna= new List<Postac>();
            Druzyna.Add(w);
            Druzyna.Add(l);
            Druzyna.Add(czarke);

            Druzyna.Sort();

            Druzyna.ForEach(p =>
            {
                Console.WriteLine(p.Imie);
            });

            w.odnoszenieObrazen(100, Obrazenia.Ogniste);
            for(int n = 0; n < 5; n++)
            {
                l.odnoszenieObrazen(1, Obrazenia.Fizyczne);
            }

            czarke.mana.Aktualne = 5;
            Console.WriteLine($"Czarek mana {czarke.ManaP.Aktualne}");
            czarke.medytuj();
            Console.WriteLine($"Czarek mana {czarke.ManaP.Aktualne}");
            leon.zionOgniem();
            var lol=Druzyna.Select(k => new { k.Imie, k.Poziom }).Where(k => k.Poziom > 1).ToList();
            lol.ForEach(k =>
            {
                Console.WriteLine($"Imie: {k.Imie} Poziom: {k.Poziom} ");
            });
            Console.ReadLine();
        }

        
    }
    partial  class Smok
    {
        /*partial  void metoda(int ilosc)
        {
            Console.WriteLine(" Dzieje sie");

        }
        */
    }
}

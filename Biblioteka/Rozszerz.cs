﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public static class Rozszerz
    {
        public static void czerwonyDrink(this Postac p)
        {
            p.ZycieP.Aktualne += 100;
        }
        public static void zyjesz(this Postac p)
        {
            if (p.ZycieP.Aktualne <= 0)
            {
                Console.WriteLine($"Postac {p.Imie} nie zyje");
            }
            else
            {
                Console.WriteLine($"Postac {p.Imie} zyje i ma {p.ZycieP.Aktualne} punktow zdrowia");
            }
        }
        public static void punktyDoswiadczenia(this Postac p)
        {
            if(p.PunktyDoswiadczenia > 1000)
            {
                p.LevelUp();
                p.PunktyDoswiadczenia = 0;
            }
        }
        public static void manaUp(this Czarodziej p)
        {
            p.ManaP.Aktualne += ((20 / 100) * p.ManaP.Maksymalne);
        }
        public static void rozbrojMnie(this Postac p, int oIle)
        {
            p.ZadawaneObrazenia -= oIle;
        }
        public static void zdejmijPancerz(this Postac p, int oIle)
        {
            p.OdpornoscP -= oIle;
        }
        public static void medytuj(this IMagiczny p)
        {
            Czarodziej t = (Czarodziej)p;
            t.ManaP.Aktualne = t.ManaP.Maksymalne;
            p = t;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public class Wojownik: Postac 
    {
        public int IloscAtakow { get; set; }

        public Wojownik(string imie) : base(imie, 0,0,10,3,3,100,new List<Biblioteka.Odpornosc> {
            new Biblioteka.Odpornosc { Nazwa="Ogien"},
            new Biblioteka.Odpornosc {Nazwa="Woda" }
        },new Zycie { Maksymalne=100, Aktualne=100},
          new Mana { Aktualne=50, Maksymalne=50},50)
        {
            IloscAtakow = 5;
        }

        public override string ToString()
        {
            return base.ToString()+ "Ilosc Atakow: "+IloscAtakow;
        }
        public override int zadawaneObrazenia()
        {
            return (base.zadawaneObrazenia()*IloscAtakow);
        }
        public override void LevelUp()
        {
            Poziom++;
            base.OdpornoscP += 200;
            base.Sila += 10;
            Zrecznosc += 3;
            ZycieP.Maksymalne += 200;
            ManaP.Maksymalne += 25;
            base.Inteligencja += 3;
        }

        public override void uzbrajanie(int wartosc)
        {
            base.uzbrajanie(wartosc);
        }
        public override void odziejSie(int ileTego)
        {
            base.odziejSie(ileTego);
        }
        public override void odnoszenieObrazen(int wartosc, Obrazenia o)
        {
            base.odnoszenieObrazen(wartosc, o);
        }
    }
}

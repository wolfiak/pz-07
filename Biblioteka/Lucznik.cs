﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public delegate void Unik(Lucznik p);
    public class Lucznik : Postac
    {

        public event Unik onUnik;
        public int SzansaNaUnikniecie { get; set; }
        public Lucznik(string imie): base(imie,0,0,3,10,3,120,new List<Biblioteka.Odpornosc> {
            new Biblioteka.Odpornosc {Nazwa="Fizyczne" }
        },new Zycie {
            Aktualne=70,
            Maksymalne=70
        },new Mana
        {
            Aktualne=50,
            Maksymalne=50
        }, 50)
        {
            SzansaNaUnikniecie = 5;
        }

        public override string ToString()
        {
            return base.ToString() + "Szansa na unikniecie: " + SzansaNaUnikniecie;
        }
        public override void odnoszenieObrazen(int wartosc, Obrazenia o)
        {
            if (Obrazenia.Fizyczne == o)
            {
                Random r = new Random();
                int i=r.Next(0, 100);
                Console.WriteLine(SzansaNaUnikniecie);
                if (SzansaNaUnikniecie >= i)
                {
                    onUnik(this);
                }
                else
                {
                    base.odnoszenieObrazen(wartosc, o);
                }
            }else if (Obrazenia.Ogniste == o)
            {
                base.odnoszenieObrazen(wartosc, o);
            }
            
        }
        public override void LevelUp()
        {
            Poziom++;
            base.OdpornoscP += 100;
            base.Sila += 3;
            Zrecznosc += 10;
            ZycieP.Maksymalne += 100;
            ManaP.Maksymalne += 50;
            base.Inteligencja += 3;
           
        }
        public override void uzbrajanie(int wartosc)
        {
            base.uzbrajanie(wartosc);
        }
        public override void odziejSie(int ileTego)
        {
            base.odziejSie(ileTego);
        }
        public override int zadawaneObrazenia()
        {
            return base.zadawaneObrazenia();
        }
    }
}

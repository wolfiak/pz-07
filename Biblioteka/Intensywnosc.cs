﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public class Intensywnosc : EventArgs
    {
        public int Iletego { get; set; }
        public Intensywnosc(int Iletego)
        {
            this.Iletego = Iletego;
        }
        public override string ToString()
        {
            return string.Format("I smok ziona iloscia ognia rowna {0}", Iletego);
        }
    }
}

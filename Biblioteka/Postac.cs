﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public delegate void PostacDelegate(Postac p);
    
    public abstract class Postac : IComparable<Postac>
    {

        //public PostacDelegate DeadNote { get; set; }

        public event PostacDelegate onDie;
        public Postac(string imie,int poziom, int punktyDoswiadczenia, int sila, int zrecznosc, int inteligencja, int zadawaneObrazenia, List<Odpornosc> odpornosc, Zycie zycie, Mana mana, int odpornoscP )
        {
            Imie = imie;
            Poziom = poziom;
            PunktyDoswiadczenia = punktyDoswiadczenia;
            Sila = sila;
            Zrecznosc = zrecznosc;
            Inteligencja = inteligencja;
            ZadawaneObrazenia = zadawaneObrazenia;
            Odpornosc = odpornosc;
            ZycieP = zycie;
            ManaP = mana;
            OdpornoscP = odpornoscP;
        }
        public int OdpornoscP { get; set; }
        public string Imie { get; set; }
        public int Poziom { get; set; }
        public int PunktyDoswiadczenia { get; set; }
        public int Sila { get; set; }
        public int Zrecznosc { get; set; }
        public int Inteligencja { get; set; }
        public int ZadawaneObrazenia { get; set; }
        public List<Odpornosc> Odpornosc { get; set; }
        public Zycie ZycieP { get; set; }
        public Mana ManaP { get; set; }

        public override string ToString()
        {
            return string.Format("Imie: {0}, Poziom: {1}, Punkty Doswiadczenia: {2}, Sila: {3}, Zrecznosc: {4}, Inteligencja {5}, ZadawaneObrazenia: {6}, Zycie Maksymalne: {7}, Zycie Aktualne: {8}, Mana Maksymalne: {9}, Mana Aktualne: {10} odpornosc:",Imie,Poziom,PunktyDoswiadczenia, Sila, Zrecznosc, Inteligencja, ZadawaneObrazenia, ZycieP.Aktualne, ZycieP.Maksymalne,ManaP.Maksymalne,ManaP.Aktualne);
          
        }
        public virtual void uzbrajanie(int wartosc)
        {
            ZadawaneObrazenia += wartosc;
        }
        public void odpornosc(int wartosc)
        {
            throw new Exception("Nie ma takiej wlasciwoscie bo dziala to inaczej");

        }
        public virtual int zadawaneObrazenia()
        {
            return ZadawaneObrazenia;
        }
        public virtual void odziejSie(int ileTego)
        {
            OdpornoscP += ileTego;
        }
        public virtual void odnoszenieObrazen(int wartosc, Obrazenia o)
        {
            Console.WriteLine($"Postac odnosi obrazenia od {o} o wartosci: {wartosc} ");
            if(o == Obrazenia.Ogniste)
            {
                ZycieP.Aktualne -= (wartosc);
                
            }
            else
            {
                Console.WriteLine($"Zycie aktualne {ZycieP.Aktualne} wartosc - odpornosc: {wartosc - OdpornoscP}");
               ZycieP.Aktualne -= (wartosc - OdpornoscP) * -1;
            }
            Console.WriteLine($"Pozostalo zycia {ZycieP.Aktualne}");
            if(ZycieP.Aktualne<= 0)
            {
                onDie(this);
            }
            //onDie(this);
        }
        public abstract  void LevelUp();

        public int Compare(Postac x, Postac y)
        {
            return x.Poziom.CompareTo(y.Poziom);
            
        }

        public int CompareTo(Postac other)
        {
            return this.Poziom.CompareTo(other.Poziom);
        }

        /* public void Umrzyj(PostacDelegate d)
         {
             d();

         }*/
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public class Czarodziej: Postac,IMagiczny
    {
        public Czarodziej(string name) :base(name,0,0,3,3,10,200,new List<Biblioteka.Odpornosc>
        {
            new Biblioteka.Odpornosc
            {
               Nazwa="ogien"
            }
        },new Zycie
        {
            Aktualne=50,
            Maksymalne=50
        },new Mana
        {
            Aktualne=200,
            Maksymalne=200
        },50)
        {

        }

        public Mana mana
        {
            get
            {
                return this.ManaP;
            }

            set
            {
                this.ManaP = value;
            }
        }

        public override void LevelUp()
        {
            Poziom++;
            base.OdpornoscP += 100;
            base.Sila += 3;
            Zrecznosc += 3;
            ZycieP.Maksymalne += 50;
            ManaP.Maksymalne += 200;
            base.Inteligencja += 10;
        }

        public void rzucCzar()
        {
            throw new NotImplementedException();
        }
        public override string ToString()
        {
        
            return base.ToString();
        }
        public override void uzbrajanie(int wartosc)
        {
            base.uzbrajanie(wartosc);
        }
        public override void odziejSie(int ileTego)
        {
            base.odziejSie(ileTego);
        }
        public override int zadawaneObrazenia()
        {
            return base.zadawaneObrazenia();
        }
    }
}

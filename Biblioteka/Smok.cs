﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public partial class Smok : Postac
    {
        public event EventHandler<Intensywnosc> onZioniecie;
       // public string imie { get; set; }
        public Smok() :base("leon",1,0,0,0,0,0,
            new List<Odpornosc>
            {
                new Biblioteka.Odpornosc{Nazwa= "dUPA"}
            }
            ,new Zycie { Aktualne=0 , Maksymalne= 100}
            ,new Mana { Aktualne=0, Maksymalne=100}
            ,0)
        {

        }
        partial void metoda(int ilosc);
        public void zionOgniem()
        {
           
            Random r = new Random();
            int iloscOgnia = r.Next(50, 100);
            metoda(iloscOgnia);
            onZioniecie(this, new Intensywnosc(iloscOgnia));
        }
        public override void LevelUp()
        {
            throw new NotImplementedException();
        }

       
    }
}
